# Select item from a list of objects by a key value in each object
cat .mozilla/firefox/XXX.default-release/logins.json | \
  jq '.logins[] | select(.hostname | contains("facebook"))'

# ...or with select()
cat .mozilla/firefox/XXX.default-release/logins.json | \
  jq '.logins[] | select(.hostname!="facebook").property'

# URL Encode a string
printf %s 'string to encode' | jq -sRr @uri
