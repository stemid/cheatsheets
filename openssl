# Classic generation of self-signed TLS cert in one line with SAN and no password.
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout example.tld.crt -subj "/CN=example.tld" -addext "subjectAltName=DNS:example.tld,DNS:*.example.tld,IP:10.11.12.13"

# The following series of commands will create an internal CA. First generate private key.
openssl genrsa -out "ca-internal.key.pem" 4096

# Generate CA signing request for Internal organisation.
openssl req -new -key ca-internal.key.pem -batch -out "ca-internal.csr" -utf8 -subj '/O=Internal'

# Generate configuration for CA cert.
cat <<EOF >ca-internal.ini
basicConstraints = critical, CA:TRUE
keyUsage = critical, keyCertSign, cRLSign
subjectKeyIdentifier = hash
nameConstraints = critical, permitted;DNS:organisation.internal, permitted;DNS:.organisation.internal
EOF

# Generate CA cert for organisation.internal domain.
openssl x509 -req -sha256 -days "3650" -in "ca-internal.csr" -signkey "ca-internal.key.pem" -extfile "ca-internal.ini" -out ca-internal.crt

# Create serial counter file.
echo 1000 > ca-internal.srl

# Generate domain-specific key.
openssl genrsa -out wildcard.organisation.internal.key 2048

# Generate domain-specific signing request.
openssl req -new -key wildcard.organisation.internal.key -batch -out wildcard.organisation.internal.csr -utf8 -subj "/CN=*.organisation.internal"

# Create wildcard certificate configuration.
echo <<EOF >wildcard.organisation.internal.ini
basicConstraints = critical, CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always
nsCertType = server
authorityKeyIdentifer = keyid, issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = ${ENV::CERT_SAN}
EOF

# Generate wildcard domain certificate for *.organisation.internal.
export CERT_SAN="DNS:organisation.internal,DNS:*.organisation.internal"
openssl x509 -req -sha256 -days "3650" -in "wildcard.organisation.internal.csr" -CAkey "ca-internal.key.pem" -CA "ca-internal.crt" -CAserial "ca-internal.srl" -out "wildcard.organisation.internal.crt" -extfile "wildcard.organisation.internal.ini"
