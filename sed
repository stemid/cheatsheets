# Make changes directly to a file with -i.
sed -iE '/Delete this line/d' filename.txt

# Delete matching line.
cat filename.txt | sed '/Delete this line/d'

# Search and replace with extended regexp and sub-group variables.
sed -E 's|\{\{< figure src="([^"]+)" alt="([^"]+)" .+ >\}\}|![\2](\1)|g' 
