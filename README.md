# Personal cheatsheets

For the [cheat](https://github.com/cheat/cheat) cli utility.

Pop these files into ``~/.config/cheat/cheatsheets/personal/`` however you feel like and you can use the ``cheat`` command to view them.

For example ``cheat -a ffmpeg`` to combine both upstream and personal cheatsheets for ``ffmpeg``.

Add your own files as you discover more cool commands.
